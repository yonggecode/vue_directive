import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/Imglazy'
  },
  {
    path: '/Imglazy',
    name: 'Imglazy',
    component:() => import('../views/Imglazy.vue')
  },
  {
    path: '/Directive',
    name: 'Directive',
    component: () => import('../views/Directive.vue')
  },
  {
    path: '/Animation',
    name: 'Animation',
    component: () => import('../views/Animation.vue')
  },
  {
    path: '/News',
    name: 'News',
    component: () => import('../views/News.vue')
  },
  {
    path: '/Mixin',
    name: 'Mixin',
    component: () => import('../views/Mixin.vue')
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
